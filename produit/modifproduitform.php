<form method="post">
  <input type="hidden" name="id" value="<?php echo $produit['id']; ?>">
  
  <label for="nom">Nom :</label>
  <input type="text" name="nom" value="<?php echo $produit['nom']; ?>"><br>
  
  <label for="imgURL">URL de l'image :</label>
  <input type="text" name="imgURL" value="<?php echo $produit['imgURL']; ?>"><br>
  
  <label for="info">Informations :</label>
  <textarea name="info"><?php echo $produit['info']; ?></textarea><br>
  
  <label for="son">Son :</label>
  <input type="text" name="son" value="<?php echo $produit['son']; ?>"><br>
  
  <label for="prix">Prix :</label>
  <input type="number" step="0.01" name="prix" value="<?php echo $produit['prix']; ?>"><br>
  
  <label for="dispo">Disponibilité :</label>
  <input type="checkbox" name="dispo" <?php if ($produit['dispo']) echo 'checked'; ?>><br>
  <input type="submit" name="submit" value="Modifier le produit">
</form>
