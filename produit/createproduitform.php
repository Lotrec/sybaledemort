
<form action="createproduit.php" method="post"  enctype="multipart/form-data">

    <label for="nom">Nom :</label>
    <input type="text" name="nom" required><br>

    <label for="imgURL">URL de l'image :</label>
    <input type="file" name="imgURL" required><br>

    <label for="info">Informations :</label>
    <textarea name="info" required></textarea><br>

    <label for="son">Son :</label>
    <input type="text" name="son" required><br>

    <label for="prix">Prix :</label>
    <input type="number" step="0.01" name="prix" required><br>

    <label for="dispo">Disponibilité :</label>
    <input type="checkbox" name="dispo"><br>

    <input type="submit" value="Créer le produit">
</form>
<!-- rajouter des "required" permet d'en faire des champs obligatoires,
le br remplace parfaitement le truc de sauvage avec les divs
le step permet de faire des crans de prix, le classico,

pour la dispo, si elle est cochée c'est que le produit est dispo, sinon il n'est pas dispo-->