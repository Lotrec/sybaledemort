DROP DATABASE IF EXISTS symbaledemort;
CREATE DATABASE symbaledemort;
USE symbaledemort;

create table produit (
    id int not null auto_increment primary key,
    nom varchar(800),
    imgURL varchar(800),
    info varchar(800),
    son varchar(800),
    prix float,
    dispo boolean
);


create table client (
id int not null auto_increment primary key,
nom varchar (800),
email varchar (800),
telephone varchar (800)
);

create table commande (
    id int not null auto_increment primary key,
    id_client int, -- ici on stock l'id du client qui commande
    foreign key (id_client) references client(id)
);

create table ligne_commande (
    id_produit int,  
    id_commande int not null,
    quantite float
);

DROP USER IF EXISTS 'root'@'127.0.0.1';
CREATE USER 'root'@'127.0.0.1' IDENTIFIED BY '';
GRANT ALL PRIVILEGES ON symbaledemort.* TO 'root'@'127.0.0.1';