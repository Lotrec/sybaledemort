<?php
include_once 'debug.php';


// connect à la db

    $host = '127.0.0.1';
    $db   = 'symbaledemort';
    $user = 'root';
    $pass = ''; // ne pas faire ça dans la vrai vie !


$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);


?>

<?php

function createclient($nom, $email, $telephone){
    global $pdo;
    $req = $pdo->prepare('insert into client(nom, email, telephone) values (?, ?, ?);');
    $req->execute([$nom, $email, $telephone]);
};

function readclient($id){
    global $pdo;
    $req = $pdo->prepare("select * from client where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
};

function readAllclient(){
    global $pdo;
    $req = $pdo->query("select * from client;");
    return $req->fetchAll();
};

function createproduit($nom, $imgURL, $info, $son ,$prix, $dispo){
    global $pdo;
    $dispo = $dispo ? 1 : 0;
    $req = $pdo->prepare('insert into produit(nom, imgURL, info, son, prix, dispo) values (?, ?, ?, ?, ?, ?);');
    $req->execute([$nom, $imgURL["name"], $info ,$son, $prix, $dispo]);
};


function updateproduit($id, $nom, $imgURL, $info, $son ,$prix, $dispo){
    global $pdo;
    $dispo = $dispo ? 1 : 0;
    $req = $pdo->prepare("update produit set nom=? imgURL=?, info=?, son=? ,prix=?, dispo=? where id=?;");
    $req->execute([$id, $nom, $imgURL, $info, $son, $prix, $dispo]);
};

function readproduit($id){
    global $pdo;
    $req = $pdo->prepare("select * from produit where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
};

function readAllproduit(){
    global $pdo;
    $req = $pdo->query("select * from produit;");
    return $req->fetchAll();
};

function createpanier($id_produit, $id_commande, $quantite){
    global $pdo;
    $req = $pdo->prepare('insert into panier(id_produit, id_commande, quantite) values (?, ?, ?);');
    $req->execute([$id_produit, $id_commande, $quantite]);
};

function readpanier($id_commande){
    global $pdo;
    $req = $pdo->prepare("select * from panier where id_commande=?;");
    $req->execute([$id_commande]);
    return $req->fetchAll();
};

function readAllpanier(){
    global $pdo;
    $req = $pdo->query("select * from panier;");
    return $req->fetchAll();
};