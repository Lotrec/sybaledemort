<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panier</title>

</head>
<body>
<?php 
$page = 'Panier';
include '../VIEW/navbar.php';
include '../MODEL/readAll.php';
?>
    <div id="liste">
    <?php
        require '../MODEL/model.php';


        foreach($reqPdtCmd as $data){
    ?>

        <div class="produit">

              

            <img src="<?= $data['image'] ?>" alt="<?= $data['nom'] ?>">
            <div>
                <h2><?= $data['nom'] ?></h2>
                <p><span><?= $data['prix'] ?></span>€</p>
            </div>
        </div>

    <?php } ?>
        <a href="validation.php">Valider mon panier</a>

    </div>
</body>
</html>