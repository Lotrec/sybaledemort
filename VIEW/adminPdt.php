<?php include '../CONTROL/isadmin.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <form action="../CONTROL/createPdt.php" method="post" enctype="multipart/form-data">

    <input type="hidden" name="id">

    <label for="nom">Nom :</label>
    <input type="text" name="nom" required><br>

    <label for="image">URL de l'image :</label>
    <input type="text" name="image"><br>

    <label for="description">Informations :</label>
    <input type="text" name="description"></textarea><br>

    <label for="son">Son :</label>
    <input type="text" name="son"><br>

    <label for="prix">Prix :</label>
    <input type="text" name="prix"><br>

    <label for="dispo">Disponibilité :</label>
    <input type="checkbox" name="dispo"><br>

    <input type="submit" value="Créer le produit">
    </form>
</body>
</html>