<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panier</title>
    <link rel="stylesheet" href="cnc.css">
</head>
<body>
<?php 

$page = 'Commandes';
include '../VIEW/navbar.php';
include '../MODEL/readAll.php';
?>
    <div id="liste">
    <?php 

$id=1;
        $req = $pdo->prepare('SELECT * from commande
        INNER JOIN produit_commande on produit_commande.id_commande = commande.id
        INNER JOIN produit ON produit_commande.id_produit = produit.id
        where commande.id = ?;');
        $req->execute([$id]);

        $repCmd = $req->fetchAll();

        foreach($repCmd as $data){
    ?>

        <div class="produit">
            <img src="<?= $data['image'] ?>" alt="<?= $data['nom'] ?>">
            <div>
                <h2><?= $data['nom'] ?></h2>
                <p><span><?= $data['prix'] ?></span>€</p>
            </div>
        </div>
    <?php } ?>
    </div>
</body>
</html>